<?php

namespace Duna\DI\Extensions;

use Duna\Console\Helper;
use Duna\Console\IEntityConsoleProvider;
use Nette\InvalidStateException;

abstract class CompilerExtension extends \Nette\DI\CompilerExtension
{
    public function addCommands(array $commands)
    {
        if (!($this instanceof IEntityConsoleProvider))
            throw new InvalidStateException('Please you have to implement interface ' . IEntityConsoleProvider::class);
        Helper::loadCommands($this, $commands);
    }

    public function addPresenterMapping(array $mapping)
    {
        $builder = $this->getContainerBuilder();
        \Duna\DI\Helper::setPresenterMapping($builder, $mapping);
    }

    public function parseConfig($path)
    {
        \Duna\DI\Helper::parseConfig($this, $this->compiler, $path);
    }

    public function addMacros(array $macros)
    {
        foreach ($macros as $macro) {
            $this->addMacro($macro);
        }
    }

    public function addMacro($installMacro)
    {
        $latteFactory = $this->getLatteFactory();
        \Duna\DI\Helper::addMacro($latteFactory, $installMacro);
    }

    /**
     * @return \Nette\DI\ServiceDefinition
     */
    public function getLatteFactory()
    {
        $builder = $this->getContainerBuilder();
        return $builder->hasDefinition('nette.latteFactory') ? $builder->getDefinition('nette.latteFactory') : $builder->getDefinition('nette.latte');
    }
}