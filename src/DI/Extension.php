<?php

namespace Duna\DI;

use Nette\DI\CompilerExtension;

/**
 * @package Duna\DI
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 */
class Extension extends CompilerExtension
{
    public function loadConfiguration()
    {
        Helper::parseConfig($this, $this->compiler, __DIR__ . '/config.neon');
    }
}
