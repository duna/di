<?php

namespace Duna\DI;

use Latte\Macros\MacroSet;
use Nette\Application\IPresenterFactory;
use Nette\DI\Compiler;
use Nette\DI\CompilerExtension;
use Nette\DI\ContainerBuilder;
use Nette\StaticClass;

class Helper
{
    use StaticClass;

    public static function parseConfig(CompilerExtension $ext, Compiler $compiler, $filename)
    {
        $builder = $ext->getContainerBuilder();
        $config = $ext->loadFromFile($filename);
        $compiler->parseServices($builder, $config);
        if (isset($config['extensions'])) {
            foreach ($config['extensions'] as $i => $extension) {
                $compiler->addExtension($i, new $extension);
            }
        }
        if (isset($config['commands'])) {
            foreach ($config['commands'] as $i => $command) {
                $builder->addDefinition($extension->prefix('cli.' . $i))
                    ->addTag(\Kdyby\Console\DI\ConsoleExtension::TAG_COMMAND)
                    ->setInject(false)
                    ->setClass($command);
            }
        }
    }


    public static function setPresenterMapping(ContainerBuilder $builder, array $mapping)
    {
        $builder->getDefinition($builder->getByType(IPresenterFactory::class))
            ->addSetup('setMapping', [$mapping]);
    }

    public static function addMacro(\Nette\DI\ServiceDefinition $latteFactory, $call)
    {
        $latteFactory->addSetup('?->onCompile[] = function($engine) { ' . $call . '::install($engine->getCompiler()); }', ['@self']);
    }
}